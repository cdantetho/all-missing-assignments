﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.mnuAreas = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShapesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CircleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RectangleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TriangleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PerimeterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RectangleToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TriangleToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PythagerousToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PythagorousToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.LstArea = New System.Windows.Forms.ListBox()
        Me.mnuAreas.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnuAreas
        '
        Me.mnuAreas.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ShapesToolStripMenuItem, Me.PerimeterToolStripMenuItem, Me.PythagerousToolStripMenuItem})
        Me.mnuAreas.Location = New System.Drawing.Point(0, 0)
        Me.mnuAreas.Name = "mnuAreas"
        Me.mnuAreas.Size = New System.Drawing.Size(570, 24)
        Me.mnuAreas.TabIndex = 0
        Me.mnuAreas.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'ShapesToolStripMenuItem
        '
        Me.ShapesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CircleToolStripMenuItem, Me.RectangleToolStripMenuItem, Me.TriangleToolStripMenuItem})
        Me.ShapesToolStripMenuItem.Name = "ShapesToolStripMenuItem"
        Me.ShapesToolStripMenuItem.Size = New System.Drawing.Size(43, 20)
        Me.ShapesToolStripMenuItem.Text = "Area"
        '
        'CircleToolStripMenuItem
        '
        Me.CircleToolStripMenuItem.Name = "CircleToolStripMenuItem"
        Me.CircleToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.CircleToolStripMenuItem.Text = "Circle"
        '
        'RectangleToolStripMenuItem
        '
        Me.RectangleToolStripMenuItem.Name = "RectangleToolStripMenuItem"
        Me.RectangleToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.RectangleToolStripMenuItem.Text = "Rectangle"
        '
        'TriangleToolStripMenuItem
        '
        Me.TriangleToolStripMenuItem.Name = "TriangleToolStripMenuItem"
        Me.TriangleToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.TriangleToolStripMenuItem.Text = "Triangle"
        '
        'PerimeterToolStripMenuItem
        '
        Me.PerimeterToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RectangleToolStripMenuItem1, Me.TriangleToolStripMenuItem1})
        Me.PerimeterToolStripMenuItem.Name = "PerimeterToolStripMenuItem"
        Me.PerimeterToolStripMenuItem.Size = New System.Drawing.Size(70, 20)
        Me.PerimeterToolStripMenuItem.Text = "Perimeter"
        '
        'RectangleToolStripMenuItem1
        '
        Me.RectangleToolStripMenuItem1.Name = "RectangleToolStripMenuItem1"
        Me.RectangleToolStripMenuItem1.Size = New System.Drawing.Size(126, 22)
        Me.RectangleToolStripMenuItem1.Text = "Rectangle"
        '
        'TriangleToolStripMenuItem1
        '
        Me.TriangleToolStripMenuItem1.Name = "TriangleToolStripMenuItem1"
        Me.TriangleToolStripMenuItem1.Size = New System.Drawing.Size(126, 22)
        Me.TriangleToolStripMenuItem1.Text = "Triangle"
        '
        'PythagerousToolStripMenuItem
        '
        Me.PythagerousToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PythagorousToolStripMenuItem})
        Me.PythagerousToolStripMenuItem.Name = "PythagerousToolStripMenuItem"
        Me.PythagerousToolStripMenuItem.Size = New System.Drawing.Size(86, 20)
        Me.PythagerousToolStripMenuItem.Text = "Pythagorous"
        '
        'PythagorousToolStripMenuItem
        '
        Me.PythagorousToolStripMenuItem.Name = "PythagorousToolStripMenuItem"
        Me.PythagorousToolStripMenuItem.Size = New System.Drawing.Size(141, 22)
        Me.PythagorousToolStripMenuItem.Text = "Pythagorous"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(293, 51)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(190, 280)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'LstArea
        '
        Me.LstArea.FormattingEnabled = True
        Me.LstArea.Location = New System.Drawing.Point(27, 79)
        Me.LstArea.Name = "LstArea"
        Me.LstArea.Size = New System.Drawing.Size(216, 225)
        Me.LstArea.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightYellow
        Me.ClientSize = New System.Drawing.Size(570, 431)
        Me.Controls.Add(Me.LstArea)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.mnuAreas)
        Me.MainMenuStrip = Me.mnuAreas
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.mnuAreas.ResumeLayout(False)
        Me.mnuAreas.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuAreas As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ShapesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CircleToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RectangleToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents LstArea As System.Windows.Forms.ListBox
    Friend WithEvents TriangleToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PerimeterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RectangleToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TriangleToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PythagerousToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PythagorousToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
