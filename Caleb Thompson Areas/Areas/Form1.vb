﻿'Name: Caleb Thompson
'Date: 11/18/2013
'Period: 3rd
'Project Areas:
' this project will calculate the areas of 2 shapes a cirlce and a rectangle
' you will select the shape from the menu then use functions to input information 
' and to calculate the areas, the area will be displayed in the listbox

'inputs: circle =>radius, rectangle =>length, width
'process: circle - use math functions Math.pi, math.pow, math.round area=pi * r^2
'process: rectangle - calculate area = length * width
'output: the area and the important metric
Public Class Form1
    'global variables
    Dim area As Double
    Dim perimeter As Double
    Dim pythagorous As Double

    Private Sub CircleToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CircleToolStripMenuItem.Click
        'local variable radius
        Dim radius As Double
        'input the radius using the function inputbox
        radius = InputBox("Enter the radius of the circle", "Calculate Cirle Area", "0.0")
        'use math functions to calculate the area rounding off to 2 decimal places
        area = Math.Round(Math.PI * Math.Pow(radius, 2), 2)
        'output the area to LstArea
        LstArea.Items.Add("The area of your circle is: " & area)

    End Sub

    Private Sub RectangleToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RectangleToolStripMenuItem.Click
        'local variables length and width
        Dim length As Double
        Dim width As Double

        'get the inputs length and width using an inputbox function
        length = InputBox("Enter your length of the rectangle", "Calculate Rectangle Area", "0")
        width = InputBox("Enter the width of the rectangle", "calculate Rectangle Area", "0")
        'process calculate the area
        area = Math.Round(length * width, 2)
        'output information to LstArea
        LstArea.Items.Add("The area of your rectangle is: " & area)
           End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        End
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click

    End Sub

    Private Sub TriangleToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TriangleToolStripMenuItem.Click
        Dim base As Double
        Dim height As Double
        base = InputBox("Enter the length of the base of your triangle", "Calculate Triangle Area", "0")
        height = InputBox("Enter the height of your triangle", "Calculate Triangle Area", "0")
        area = Math.Round(base * height / 2, 3)
        LstArea.Items.Add("The area of your triangle is: " & area)
    End Sub

    Private Sub ShapesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShapesToolStripMenuItem.Click

    End Sub

    Private Sub TriangleToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles TriangleToolStripMenuItem1.Click
        Dim side1 As Double
        Dim side2 As Double
        Dim side3 As Double
        side1 = InputBox("Enter the length of the 1st side of your triangle", "Calculate Triangle Perimeter", "0")
        side2 = InputBox("Enter the length of the 2nd side of your triangle", "Calculate Triangle Perimeter", "0")
        side3 = InputBox("Enter the length of the 3rd side of your triangle", "Calculate Triangle Perimeter", "0")
        perimeter = Math.Round(side1 + side2 + side3, 3)
        LstArea.Items.Add("The perimeter of your triangle is: " & perimeter)
    End Sub

    Private Sub RectangleToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles RectangleToolStripMenuItem1.Click
        Dim side1 As Double
        Dim side2 As Double
        Dim side3 As Double
        Dim side4 As Double
        side1 = InputBox("Enter the length of the 1st side of your rectangle", "Calculate Rectangle Perimeter", "0")
        side2 = InputBox("Enter the length of the 2nd side of your rectangle", "Calculate Rectangle Perimeter", "0")
        side3 = InputBox("Enter the length of the 3rd side of your rectangle", "Calculate Rectangle Perimeter", "0")
        side4 = InputBox("Enter the length of the 3rd side of your rectangle", "Calculate Rectangle Perimeter", "0")
        perimeter = Math.Round(side1 + side2 + side3 + side4, 3)
        LstArea.Items.Add("The perimeter of your triangle is: " & perimeter)
    End Sub

    Private Sub PythagorousToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PythagorousToolStripMenuItem.Click
        Dim B As Integer
        Dim a As Integer
        Dim C As Double

        a = InputBox("Enter the length of the 1st side of your triangle", "Calculate Pythagorous", "0")
        B = InputBox("Enter the length of the 1st side of your triangle", "Calculate Pythagorous", "0")
        C = a ^ 2 + B ^ 2
        pythagorous = Math.Round(Math.Sqrt(C), 3)
        LstArea.Items.Add("The hypotenuse of your triangle is: " & pythagorous)
    End Sub

    Private Sub LstArea_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LstArea.SelectedIndexChanged

    End Sub
End Class
