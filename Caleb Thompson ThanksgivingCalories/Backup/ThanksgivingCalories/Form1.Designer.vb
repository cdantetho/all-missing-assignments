﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.tukey = New System.Windows.Forms.PictureBox
        Me.mnu1 = New System.Windows.Forms.CheckBox
        Me.mnu2 = New System.Windows.Forms.CheckBox
        Me.mnu3 = New System.Windows.Forms.CheckBox
        Me.mnu4 = New System.Windows.Forms.CheckBox
        Me.mnu5 = New System.Windows.Forms.CheckBox
        Me.mnu6 = New System.Windows.Forms.CheckBox
        Me.mnu7 = New System.Windows.Forms.CheckBox
        Me.btnCalculate = New System.Windows.Forms.Button
        Me.lblTitle = New System.Windows.Forms.Label
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser
        CType(Me.tukey, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tukey
        '
        Me.tukey.Image = CType(resources.GetObject("tukey.Image"), System.Drawing.Image)
        Me.tukey.Location = New System.Drawing.Point(130, 77)
        Me.tukey.Name = "tukey"
        Me.tukey.Size = New System.Drawing.Size(187, 205)
        Me.tukey.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.tukey.TabIndex = 0
        Me.tukey.TabStop = False
        '
        'mnu1
        '
        Me.mnu1.AutoSize = True
        Me.mnu1.Location = New System.Drawing.Point(25, 56)
        Me.mnu1.Name = "mnu1"
        Me.mnu1.Size = New System.Drawing.Size(81, 17)
        Me.mnu1.TabIndex = 1
        Me.mnu1.Text = "CheckBox1"
        Me.mnu1.UseVisualStyleBackColor = True
        '
        'mnu2
        '
        Me.mnu2.AutoSize = True
        Me.mnu2.Location = New System.Drawing.Point(25, 88)
        Me.mnu2.Name = "mnu2"
        Me.mnu2.Size = New System.Drawing.Size(81, 17)
        Me.mnu2.TabIndex = 1
        Me.mnu2.Text = "CheckBox1"
        Me.mnu2.UseVisualStyleBackColor = True
        '
        'mnu3
        '
        Me.mnu3.AutoSize = True
        Me.mnu3.Location = New System.Drawing.Point(25, 126)
        Me.mnu3.Name = "mnu3"
        Me.mnu3.Size = New System.Drawing.Size(81, 17)
        Me.mnu3.TabIndex = 1
        Me.mnu3.Text = "CheckBox1"
        Me.mnu3.UseVisualStyleBackColor = True
        '
        'mnu4
        '
        Me.mnu4.AutoSize = True
        Me.mnu4.Location = New System.Drawing.Point(25, 165)
        Me.mnu4.Name = "mnu4"
        Me.mnu4.Size = New System.Drawing.Size(81, 17)
        Me.mnu4.TabIndex = 1
        Me.mnu4.Text = "CheckBox1"
        Me.mnu4.UseVisualStyleBackColor = True
        '
        'mnu5
        '
        Me.mnu5.AutoSize = True
        Me.mnu5.Location = New System.Drawing.Point(25, 204)
        Me.mnu5.Name = "mnu5"
        Me.mnu5.Size = New System.Drawing.Size(81, 17)
        Me.mnu5.TabIndex = 1
        Me.mnu5.Text = "CheckBox1"
        Me.mnu5.UseVisualStyleBackColor = True
        '
        'mnu6
        '
        Me.mnu6.AutoSize = True
        Me.mnu6.Location = New System.Drawing.Point(25, 242)
        Me.mnu6.Name = "mnu6"
        Me.mnu6.Size = New System.Drawing.Size(81, 17)
        Me.mnu6.TabIndex = 1
        Me.mnu6.Text = "CheckBox1"
        Me.mnu6.UseVisualStyleBackColor = True
        '
        'mnu7
        '
        Me.mnu7.AutoSize = True
        Me.mnu7.Location = New System.Drawing.Point(25, 276)
        Me.mnu7.Name = "mnu7"
        Me.mnu7.Size = New System.Drawing.Size(81, 17)
        Me.mnu7.TabIndex = 1
        Me.mnu7.Text = "CheckBox1"
        Me.mnu7.UseVisualStyleBackColor = True
        '
        'btnCalculate
        '
        Me.btnCalculate.Location = New System.Drawing.Point(182, 320)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(287, 50)
        Me.btnCalculate.TabIndex = 2
        Me.btnCalculate.Text = "Calculate Calories"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Mistral", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(95, 11)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(357, 42)
        Me.lblTitle.TabIndex = 3
        Me.lblTitle.Text = "Thanksgiving Menu Calculator"
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Location = New System.Drawing.Point(351, 66)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(372, 248)
        Me.WebBrowser1.TabIndex = 4
        Me.WebBrowser1.Url = New System.Uri("http://walking.about.com/library/cal/blthanksgivingcalories.htm", System.UriKind.Absolute)
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Red
        Me.ClientSize = New System.Drawing.Size(735, 456)
        Me.Controls.Add(Me.WebBrowser1)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.btnCalculate)
        Me.Controls.Add(Me.mnu7)
        Me.Controls.Add(Me.mnu6)
        Me.Controls.Add(Me.mnu5)
        Me.Controls.Add(Me.mnu4)
        Me.Controls.Add(Me.mnu3)
        Me.Controls.Add(Me.mnu2)
        Me.Controls.Add(Me.mnu1)
        Me.Controls.Add(Me.tukey)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.tukey, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tukey As System.Windows.Forms.PictureBox
    Friend WithEvents mnu1 As System.Windows.Forms.CheckBox
    Friend WithEvents mnu2 As System.Windows.Forms.CheckBox
    Friend WithEvents mnu3 As System.Windows.Forms.CheckBox
    Friend WithEvents mnu4 As System.Windows.Forms.CheckBox
    Friend WithEvents mnu5 As System.Windows.Forms.CheckBox
    Friend WithEvents mnu6 As System.Windows.Forms.CheckBox
    Friend WithEvents mnu7 As System.Windows.Forms.CheckBox
    Friend WithEvents btnCalculate As System.Windows.Forms.Button
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser

End Class
