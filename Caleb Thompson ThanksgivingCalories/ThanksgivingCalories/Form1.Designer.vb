﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.tukey = New System.Windows.Forms.PictureBox()
        Me.turkey = New System.Windows.Forms.CheckBox()
        Me.Mashedpotatoes = New System.Windows.Forms.CheckBox()
        Me.applercider = New System.Windows.Forms.CheckBox()
        Me.stuffing = New System.Windows.Forms.CheckBox()
        Me.augratin = New System.Windows.Forms.CheckBox()
        Me.mac = New System.Windows.Forms.CheckBox()
        Me.icream = New System.Windows.Forms.CheckBox()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.lstcalories = New System.Windows.Forms.ListBox()
        CType(Me.tukey, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tukey
        '
        Me.tukey.Image = CType(resources.GetObject("tukey.Image"), System.Drawing.Image)
        Me.tukey.Location = New System.Drawing.Point(158, 75)
        Me.tukey.Name = "tukey"
        Me.tukey.Size = New System.Drawing.Size(187, 205)
        Me.tukey.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.tukey.TabIndex = 0
        Me.tukey.TabStop = False
        '
        'turkey
        '
        Me.turkey.AutoSize = True
        Me.turkey.Location = New System.Drawing.Point(25, 56)
        Me.turkey.Name = "turkey"
        Me.turkey.Size = New System.Drawing.Size(59, 17)
        Me.turkey.TabIndex = 1
        Me.turkey.Text = "Turkey"
        Me.turkey.UseVisualStyleBackColor = True
        '
        'Mashedpotatoes
        '
        Me.Mashedpotatoes.AutoSize = True
        Me.Mashedpotatoes.Location = New System.Drawing.Point(25, 88)
        Me.Mashedpotatoes.Name = "Mashedpotatoes"
        Me.Mashedpotatoes.Size = New System.Drawing.Size(108, 17)
        Me.Mashedpotatoes.TabIndex = 1
        Me.Mashedpotatoes.Text = "Mashed potatoes"
        Me.Mashedpotatoes.UseVisualStyleBackColor = True
        '
        'applercider
        '
        Me.applercider.AutoSize = True
        Me.applercider.Location = New System.Drawing.Point(25, 126)
        Me.applercider.Name = "applercider"
        Me.applercider.Size = New System.Drawing.Size(79, 17)
        Me.applercider.TabIndex = 1
        Me.applercider.Text = "Apple cider"
        Me.applercider.UseVisualStyleBackColor = True
        '
        'stuffing
        '
        Me.stuffing.AutoSize = True
        Me.stuffing.Location = New System.Drawing.Point(25, 165)
        Me.stuffing.Name = "stuffing"
        Me.stuffing.Size = New System.Drawing.Size(62, 17)
        Me.stuffing.TabIndex = 1
        Me.stuffing.Text = "Stuffing"
        Me.stuffing.UseVisualStyleBackColor = True
        '
        'augratin
        '
        Me.augratin.AutoSize = True
        Me.augratin.Location = New System.Drawing.Point(25, 204)
        Me.augratin.Name = "augratin"
        Me.augratin.Size = New System.Drawing.Size(112, 17)
        Me.augratin.TabIndex = 1
        Me.augratin.Text = "Au gratin potatoes"
        Me.augratin.UseVisualStyleBackColor = True
        '
        'mac
        '
        Me.mac.AutoSize = True
        Me.mac.Location = New System.Drawing.Point(25, 242)
        Me.mac.Name = "mac"
        Me.mac.Size = New System.Drawing.Size(129, 17)
        Me.mac.TabIndex = 1
        Me.mac.Text = "Macaroni and cheese"
        Me.mac.UseVisualStyleBackColor = True
        '
        'icream
        '
        Me.icream.AutoSize = True
        Me.icream.Location = New System.Drawing.Point(25, 276)
        Me.icream.Name = "icream"
        Me.icream.Size = New System.Drawing.Size(73, 17)
        Me.icream.TabIndex = 1
        Me.icream.Text = "Ice cream"
        Me.icream.UseVisualStyleBackColor = True
        '
        'btnCalculate
        '
        Me.btnCalculate.Location = New System.Drawing.Point(223, 319)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(287, 50)
        Me.btnCalculate.TabIndex = 2
        Me.btnCalculate.Text = "Calculate Calories"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Mistral", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(95, 11)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(357, 42)
        Me.lblTitle.TabIndex = 3
        Me.lblTitle.Text = "Thanksgiving Menu Calculator"
        '
        'lstcalories
        '
        Me.lstcalories.FormattingEnabled = True
        Me.lstcalories.Location = New System.Drawing.Point(361, 75)
        Me.lstcalories.Name = "lstcalories"
        Me.lstcalories.Size = New System.Drawing.Size(362, 238)
        Me.lstcalories.TabIndex = 4
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Red
        Me.ClientSize = New System.Drawing.Size(735, 456)
        Me.Controls.Add(Me.lstcalories)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.btnCalculate)
        Me.Controls.Add(Me.icream)
        Me.Controls.Add(Me.mac)
        Me.Controls.Add(Me.augratin)
        Me.Controls.Add(Me.stuffing)
        Me.Controls.Add(Me.applercider)
        Me.Controls.Add(Me.Mashedpotatoes)
        Me.Controls.Add(Me.turkey)
        Me.Controls.Add(Me.tukey)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.tukey, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tukey As System.Windows.Forms.PictureBox
    Friend WithEvents turkey As System.Windows.Forms.CheckBox
    Friend WithEvents Mashedpotatoes As System.Windows.Forms.CheckBox
    Friend WithEvents applercider As System.Windows.Forms.CheckBox
    Friend WithEvents stuffing As System.Windows.Forms.CheckBox
    Friend WithEvents augratin As System.Windows.Forms.CheckBox
    Friend WithEvents mac As System.Windows.Forms.CheckBox
    Friend WithEvents icream As System.Windows.Forms.CheckBox
    Friend WithEvents btnCalculate As System.Windows.Forms.Button
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents lstcalories As System.Windows.Forms.ListBox

End Class
