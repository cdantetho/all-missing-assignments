﻿
'Name:
'Date:
'Project: Simple Grades
'    This daily question will be to complete the code for a project shell.  The project shell is located in the courses folder you will open the courses folder and then the computer programming 1 folder.
'The project we will complete today is called gradecalculation

'    'This project will input a quiz score, quiz title,  the grade scale for an A 93 – 100, B 83 – 92, C 73 – 82, D 72 – 60, F below 60

'The process of this project is for the code to determine the correct grade that should be assigned.

'The output will be the quiz title, quiz score and the letter grade. As well as a message from the teacher based on the score.

Public Class Form1
    'Variable Dictionary Declare variables for student name, test title, test score
    Dim studentname As String
    Dim testtitle As String
    Dim testscore As Integer
    'declare variable to hold the letter grade


    Private Sub btnCalculate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalculate.Click
        'get the inputs
        'get name from txtname
        studentname = txtname.Text
        'get test title from txttitle
        testtitle = txttitle.Text
        'get the score from txtscore
        testscore = txtscore.Text
        'process ---
        'determine the letter grade based on the test score

        LstReport.Items.Add(studentname)
        Select testscore
            Case 90 To 100
                LstReport.Items.Add("You have an A")
            Case 80 To 89
                LstReport.Items.Add("You have a B")
            Case 70 To 79
                LstReport.Items.Add("You have a C")
            Case 60 To 69
                LstReport.Items.Add("You have a D")
            Case 0 To 59
                LstReport.Items.Add("You have an F")
        End Select
        LstReport.Items.Add("on your " & testtitle & " test")
        'output----
        'output the student name, test name and letter grade to the list box

        'listbox name LstReport



    End Sub

    Private Sub LstReport_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LstReport.SelectedIndexChanged

    End Sub
End Class
