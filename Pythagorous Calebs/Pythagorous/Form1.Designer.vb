﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.btnCompute = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.txtsideA = New System.Windows.Forms.TextBox()
        Me.txtsideB = New System.Windows.Forms.TextBox()
        Me.txtsideC = New System.Windows.Forms.TextBox()
        Me.lblA = New System.Windows.Forms.Label()
        Me.lblB = New System.Windows.Forms.Label()
        Me.lblC = New System.Windows.Forms.Label()
        Me.lblQuestion = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCompute
        '
        Me.btnCompute.BackColor = System.Drawing.Color.Thistle
        Me.btnCompute.Location = New System.Drawing.Point(266, 314)
        Me.btnCompute.Name = "btnCompute"
        Me.btnCompute.Size = New System.Drawing.Size(228, 23)
        Me.btnCompute.TabIndex = 0
        Me.btnCompute.Text = "Compute Hypotenuse"
        Me.btnCompute.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(266, 78)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(239, 202)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.Color.Thistle
        Me.btnExit.Location = New System.Drawing.Point(385, 379)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 23)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'txtsideA
        '
        Me.txtsideA.Font = New System.Drawing.Font("Papyrus", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsideA.Location = New System.Drawing.Point(164, 120)
        Me.txtsideA.Multiline = True
        Me.txtsideA.Name = "txtsideA"
        Me.txtsideA.Size = New System.Drawing.Size(84, 36)
        Me.txtsideA.TabIndex = 3
        Me.txtsideA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtsideB
        '
        Me.txtsideB.Font = New System.Drawing.Font("Papyrus", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsideB.Location = New System.Drawing.Point(164, 184)
        Me.txtsideB.Multiline = True
        Me.txtsideB.Name = "txtsideB"
        Me.txtsideB.Size = New System.Drawing.Size(84, 36)
        Me.txtsideB.TabIndex = 3
        Me.txtsideB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtsideC
        '
        Me.txtsideC.Enabled = False
        Me.txtsideC.Font = New System.Drawing.Font("Papyrus", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsideC.Location = New System.Drawing.Point(164, 244)
        Me.txtsideC.Multiline = True
        Me.txtsideC.Name = "txtsideC"
        Me.txtsideC.Size = New System.Drawing.Size(84, 36)
        Me.txtsideC.TabIndex = 3
        Me.txtsideC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblA
        '
        Me.lblA.AutoSize = True
        Me.lblA.Font = New System.Drawing.Font("Papyrus", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblA.Location = New System.Drawing.Point(31, 120)
        Me.lblA.Name = "lblA"
        Me.lblA.Size = New System.Drawing.Size(84, 30)
        Me.lblA.TabIndex = 4
        Me.lblA.Text = "Side A"
        '
        'lblB
        '
        Me.lblB.AutoSize = True
        Me.lblB.Font = New System.Drawing.Font("Papyrus", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblB.Location = New System.Drawing.Point(31, 184)
        Me.lblB.Name = "lblB"
        Me.lblB.Size = New System.Drawing.Size(85, 30)
        Me.lblB.TabIndex = 4
        Me.lblB.Text = "Side B"
        '
        'lblC
        '
        Me.lblC.AutoSize = True
        Me.lblC.Font = New System.Drawing.Font("Papyrus", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblC.Location = New System.Drawing.Point(31, 247)
        Me.lblC.Name = "lblC"
        Me.lblC.Size = New System.Drawing.Size(127, 30)
        Me.lblC.TabIndex = 4
        Me.lblC.Text = "Hypotenuse"
        '
        'lblQuestion
        '
        Me.lblQuestion.AutoSize = True
        Me.lblQuestion.Font = New System.Drawing.Font("Papyrus", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQuestion.Location = New System.Drawing.Point(245, 45)
        Me.lblQuestion.Name = "lblQuestion"
        Me.lblQuestion.Size = New System.Drawing.Size(280, 30)
        Me.lblQuestion.TabIndex = 5
        Me.lblQuestion.Text = "Find My Hypotenuse Please!"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(571, 452)
        Me.Controls.Add(Me.lblQuestion)
        Me.Controls.Add(Me.lblC)
        Me.Controls.Add(Me.lblB)
        Me.Controls.Add(Me.lblA)
        Me.Controls.Add(Me.txtsideC)
        Me.Controls.Add(Me.txtsideB)
        Me.Controls.Add(Me.txtsideA)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.btnCompute)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCompute As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents txtsideA As System.Windows.Forms.TextBox
    Friend WithEvents txtsideB As System.Windows.Forms.TextBox
    Friend WithEvents txtsideC As System.Windows.Forms.TextBox
    Friend WithEvents lblA As System.Windows.Forms.Label
    Friend WithEvents lblB As System.Windows.Forms.Label
    Friend WithEvents lblC As System.Windows.Forms.Label
    Friend WithEvents lblQuestion As System.Windows.Forms.Label

End Class
