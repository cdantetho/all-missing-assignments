﻿'Name Caleb Thompson
'Date 11/18/2013
'Write the code below to compute the side of a hypotenuse using the pythagorean theorem a^2 + b^2 = c^2.
'to find c you will need to take the square root of a^2 + b^2
'to do this you will need to declare integer variables for a and b and a double variable for c
'look at the notes for math on my website http://www.everettsd.org/Page/10343

Public Class Form1
    'declare your variables here
    Dim B As Integer
    Dim a As Integer
    Dim C As Double

    Private Sub btnCompute_Click(sender As Object, e As EventArgs) Handles btnCompute.Click
        'input the lengths from side a and side b here
        a = txtsideA.Text
        B = txtsideB.Text


        'process calculate the value of c
        C = a ^ 2 + B ^ 2
        'output the value of c to the textbox txtsideC
        txtsideC.Text = Math.Sqrt(C)

    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        'write the code for the exit button ;P
        End

    End Sub

    Private Sub txtsideA_TextChanged(sender As Object, e As EventArgs) Handles txtsideA.TextChanged

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub txtsideB_TextChanged(sender As Object, e As EventArgs) Handles txtsideB.TextChanged

    End Sub

    Private Sub lblQuestion_Click(sender As Object, e As EventArgs) Handles lblQuestion.Click

    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click

    End Sub
End Class